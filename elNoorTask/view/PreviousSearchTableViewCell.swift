//
//  PreviousSearchTableViewCell.swift
//  elNoorTask
//
//  Created by Mac on 8/16/19.
//  Copyright © 2019 Eslam. All rights reserved.
//

import UIKit

class PreviousSearchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var hotelLabel : UILabel!
    @IBOutlet weak var banksLabel : UILabel!
    @IBOutlet weak var mosquesLabel : UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
