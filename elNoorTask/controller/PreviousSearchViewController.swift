//
//  PreviousSearchViewController.swift
//  elNoorTask
//
//  Created by Mac on 8/16/19.
//  Copyright © 2019 Eslam. All rights reserved.
//

import UIKit
import Firebase

class PreviousSearchViewController: UIViewController {
    
    @IBOutlet weak var tableView : UITableView!
    let ref = Database.database().reference(withPath: "result")
    var items: [SearchResult] = [] {
        didSet{
            self.tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        getRecomendedPlaces()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        getRecomendedPlaces()
    }
    func getRecomendedPlaces (){
        ref.observe(.value, with: { snapshot in
            var newItems: [SearchResult] = []
            //print (snapshot.childrenCount)
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot,
                    let groceryItem = SearchResult(snapshot: snapshot) {
                    print(snapshot.children)
                    newItems.append(groceryItem)
                }
            }
            self.items = newItems
            print(newItems.count)

        })
    }
    
}
extension PreviousSearchViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print(items.count)
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PreviousSearchTableViewCell", for: indexPath) as! PreviousSearchTableViewCell
        
        cell.hotelLabel.text = self.items[indexPath.row].name
        if self.items[indexPath.row].bankCount != -1 {
            cell.banksLabel.text = "this place have \(self.items[indexPath.row].bankCount) bank around 5 kilometers"
        }else {
            cell.banksLabel.text = "this place have undefined number of banks yet"
        }
        if self.items[indexPath.row].mosqueCount != -1 {
            cell.mosquesLabel.text = "and have \(self.items[indexPath.row].mosqueCount) bank around 5 kilometers"
        }else {
            cell.mosquesLabel.text = "and have undefined number of mosques yet"
        }
        

        //cell.hotelLabel.text = "this place have \(self.items[indexPath.row].mosqueCount) bank around 5 kilometers"
        return cell
    }
}

