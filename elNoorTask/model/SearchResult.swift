//
//  PlaceMarker.swift
//  elNoorTask
//
//  Created by Eslam  on 4/25/19.
//  Copyright © 2019 Eslam. All rights reserved.
//
import Foundation
import Firebase

struct SearchResult {
  
  let ref: DatabaseReference?
  let key: String?
  let bankCount: Int
  var mosqueCount: Int
  var name: String

  init?(snapshot: DataSnapshot) {
    guard
      let value = snapshot.value as? [String: AnyObject] else {
      return nil
    }
    
    let bankCount = value["bank"] as? Int
    let mosqueCount = value["mosque"] as? Int
    let name = value["name"] as? String

    self.ref = snapshot.ref
    self.key = snapshot.key
    self.bankCount = bankCount ?? -1
    self.mosqueCount = mosqueCount ?? -1
    self.name = name ?? "Error"
  }

}
